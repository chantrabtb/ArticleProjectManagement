package com.choeunchantra.home;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewSpringHomeWorkApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewSpringHomeWorkApplication.class, args);
	}
}
