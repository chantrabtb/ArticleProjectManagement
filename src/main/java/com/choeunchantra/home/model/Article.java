package com.choeunchantra.home.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class Article {
	
	public Category category;
	public int id;
	
	@NotEmpty
	public String content;
	@NotEmpty
	public String description;
	@NotEmpty
	public String author;
	
	public String createDate;
	public String image;
	
	public Article() {
		category = new Category();
	}
	
	
	public Article(int id, String content, String description, String author, String createDate, Category category,String image ) {
		super();
		this.id = id;
		this.content = content;
		this.description = description;
		this.author = author;
		this.createDate = createDate;
		this.category = category;
		this.image = image;
	}
	
	

	public Category getCategory() {
		return category;
	}


	public void setCategory(Category category) {
		this.category = category;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
}
