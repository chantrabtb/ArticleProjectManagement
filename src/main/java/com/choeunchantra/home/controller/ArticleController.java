package com.choeunchantra.home.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.choeunchantra.home.model.Article;
import com.choeunchantra.home.repository.ArticleRepository;
import com.choeunchantra.home.service.ArticleService;
import com.choeunchantra.home.service.CategoryService;

@Controller
public class ArticleController {

	@Autowired
	private ArticleService articleService;
	
	@Autowired
	private CategoryService categoryService;
	
	
	@RequestMapping(value= {"/","/index"})
	public String index( ModelMap model) {
		model.addAttribute("articles", articleService.getAllArticles() );
		return "index";
	}
	
	@RequestMapping("/deletecontent/{id}")
	public String deleteContent( @PathVariable("id") int id) {
		articleService.deleteArticle(id);
		return "redirect:/index";
	}
	
	
	@RequestMapping("/viewsingleimage/{id}")
	public String viewSingImage( @PathVariable("id") int id) {
		return "redirect:/index";
	}
	

	@RequestMapping("/updatecontent/{id}")
	public String updateContent( @PathVariable("id") int id, ModelMap model) {
		model.addAttribute("article", articleService.getArticle(id));
		model.addAttribute("categories", categoryService.getAllCategory() );
		System.out.println("This is update");
		return "html/update";
	}
		
	@PostMapping("/updateData")
	public String updateArticle(@ModelAttribute Article article , @RequestParam("file") MultipartFile file) {

		String uploadPath = "/opt/images/";
		File path = new File(uploadPath );
		
		String fileName = file.getOriginalFilename();
        String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
        fileName = UUID.randomUUID() + "." + extension;
         
        
		if( !path.exists() ) {
			path.mkdirs();
		}
	
		if( file.isEmpty() ) {

			article.setImage( articleService.getArticle( article.getId() ).getImage() );
        }else {
        	try {
				Files.copy(file.getInputStream(), Paths.get(uploadPath, fileName ));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	article.setImage( fileName );
        }
		
		article.setCategory(categoryService.getCategry(article.getCategory().getId() ));
		articleService.updateArticle(article);
		System.out.println( article.getImage() );
		return "redirect:/index";
	}
	
	
	@GetMapping("/showcontent/{id}")
	public String showContent( @PathVariable("id")int id, ModelMap model) {
		model.addAttribute("article", articleService.getArticle(id));
		return "html/view";
	}
	
	@GetMapping("/addData")
	public String addContent( ModelMap model ) {
		model.addAttribute("article", new Article() );
		model.addAttribute("categories", categoryService.getAllCategory() );
		return "html/addContent";
	}
	

	@PostMapping("/addData")
	public String saveData(@Valid @ModelAttribute Article article , BindingResult result, @RequestParam("file") MultipartFile file,  ModelMap model) {
		
		
		if(result.hasErrors() ) {
			model.addAttribute("categories", categoryService.getAllCategory() );
			model.addAttribute("article", article);
			return "html/addContent";
		}
		
		
		String uploadPath = "/opt/images/";
		File path = new File(uploadPath );
		
		String fileName = file.getOriginalFilename();
        String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
        fileName = UUID.randomUUID() + "." + extension;
        
		
		if( !path.exists() )
		{	
			path.mkdirs();
		}
		
		if( file.isEmpty() ) {
			
			article.setImage("picture.jpg");
        }else {
        	try {
				Files.copy(file.getInputStream(), Paths.get(uploadPath, fileName ));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	article.setImage( fileName );
        }
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = LocalDate.now();
		article.setCreateDate(dtf.format(localDate));
		article.setId( ArticleRepository.id);
		article.setCategory(categoryService.getCategry(article.getCategory().getId() ));
		articleService.addArticle(article);
		return "redirect:/index";
	}
}
