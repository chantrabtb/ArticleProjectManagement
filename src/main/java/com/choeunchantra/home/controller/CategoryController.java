package com.choeunchantra.home.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.choeunchantra.home.model.Category;
import com.choeunchantra.home.repository.CategoryRrepository;
import com.choeunchantra.home.service.CategoryService;

@Controller
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(value = {"/","/home"})
	public String home( ModelMap model ) {
		model.addAttribute("categories", categoryService.getAllCategory() );
		return "/html/category-home";
	}
	
	@RequestMapping("/deletecategory/{id}")
	public String deleteCategory( @PathVariable("id") int id) {
		categoryService.deleteCategory(id);
		System.out.println("delete data");
		return "redirect:/";
	}
	
	
	
	@RequestMapping("/updatecategory/{id}")
	public String updateContent( @PathVariable("id") int id, ModelMap model) {
		model.addAttribute("category", categoryService.getCategry(id));
		return "/html/updateCategory";
	}
		
	@PostMapping("/updatecatagory")
	public String updateArticle(@ModelAttribute Category category) {
		Category tmpCategory = new Category( category.getId(), category.getName(), category.getDescription() );
		categoryService.updateCategory(tmpCategory );
		return "redirect:/category/";
	}

	@RequestMapping("/addcategory")
	public String addCategory() {
		return "/html/addCategory";
	}
	
	
	@PostMapping("/addcategory")
	public String addCategoryData( @ModelAttribute Category category) {
		category.setId( CategoryRrepository.id );
		categoryService.addCategory(category);
		return "redirect:/category/";
	}
	
}


