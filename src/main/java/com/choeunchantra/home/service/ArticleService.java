package com.choeunchantra.home.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choeunchantra.home.model.Article;
import com.choeunchantra.home.repository.ArticleRepository;
import com.choeunchantra.home.repository.IArticleRepository;

@Service
public class ArticleService implements IArticleService{

	@Autowired
	IArticleRepository articleRepository;
	
	@Override
	public Article getArticle(int id) {
		// TODO Auto-generated method stub
		return articleRepository.getArticle(id);
	}

	@Override
	public void updateArticle(Article article) {
		articleRepository.update(article);
	}
	
	@Override
	public List<Article> getAllArticles() {
		return articleRepository.getAllArticles();
	}

	@Override
	public void deleteArticle(int id) {
		articleRepository.deleteArticle(id);
	}

	@Override
	public void addArticle(Article article) {
		articleRepository.addArticle(article);
	}
}
