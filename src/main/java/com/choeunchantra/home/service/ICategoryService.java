package com.choeunchantra.home.service;

import java.util.List;

import com.choeunchantra.home.model.Category;

public interface ICategoryService {
	Category getCategry( int id );
	List<Category> getAllCategory();
	void deleteCategory( int id );
	void updateCategory( Category category );
	void addCategory( Category category);

}
