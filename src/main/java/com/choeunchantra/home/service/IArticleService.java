package com.choeunchantra.home.service;

import java.util.List;

import com.choeunchantra.home.model.Article;

public interface IArticleService {

	Article getArticle( int id );
	List<Article> getAllArticles();
	void deleteArticle( int id );
	void addArticle( Article article );
	void updateArticle( Article article );
}
