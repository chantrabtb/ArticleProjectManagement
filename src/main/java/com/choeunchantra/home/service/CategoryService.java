package com.choeunchantra.home.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.choeunchantra.home.model.Category;
import com.choeunchantra.home.repository.CategoryRrepository;
import com.choeunchantra.home.repository.ICategoryRepository;


@Service
public class CategoryService implements ICategoryService{

	@Autowired
	private ICategoryRepository categoryRrepository;
	
	@Override
	public Category getCategry(int id) {
		// TODO Auto-generated method stub
		return categoryRrepository.getCategry(id);
	}

	@Override
	public List<Category> getAllCategory() {
		// TODO Auto-generated method stub
		return categoryRrepository.getAllCategory();
	}

	@Override
	public void deleteCategory(int id) {
		// TODO Auto-generated method stub
		categoryRrepository.deleteCategory(id);
	}

	@Override
	public void updateCategory(Category category) {
		// TODO Auto-generated method stub
		categoryRrepository.updateCategory(category);
	}

	@Override
	public void addCategory(Category category) {
		// TODO Auto-generated method stub
		categoryRrepository.addCategory(category);
	}

}
