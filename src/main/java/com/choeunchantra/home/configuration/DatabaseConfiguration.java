package com.choeunchantra.home.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class DatabaseConfiguration {

	@Bean
	@Profile("production")
	public DataSource dataSource() {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
		driverManagerDataSource.setUrl("jdbc:postgresql://localhost:5432/database1");
		driverManagerDataSource.setUsername("postgres");
		driverManagerDataSource.setPassword("08101996");
		return driverManagerDataSource;
	}
	
	
	//H2 Database
	@Bean
	@Profile("memory")
	public DataSource inMemoryDatabase() {
		EmbeddedDatabaseBuilder databaseBuilder = new EmbeddedDatabaseBuilder();
		databaseBuilder.setType( EmbeddedDatabaseType.H2 );
		databaseBuilder.addScript("sql/tables.sql");
		return databaseBuilder.build();
	}
}
