package com.choeunchantra.home.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.choeunchantra.home.model.Category;

@Repository
public interface ICategoryRepository {
	
	
	@Select("SELECT category_id, name, description"
			+ " from tb_categories WHERE category_id = #{id}")
	
	@Results({
			@Result(property="id",column="category_id"),
			@Result(property="name",column="name"),
			@Result(property="description",column="description")
	})
	Category getCategry( int id );
	
	
	@Select("SELECT category_id, name, description"
			+ " from tb_categories")
	
	@Results({
			@Result(property="id",column="category_id"),
			@Result(property="name",column="name"),
			@Result(property="description",column="description")
	})
	List<Category> getAllCategory();
	
	@Delete("DELETE from tb_categories WHERE category_id = #{id}")
	void deleteCategory( int id );
	
	@Update("UPDATE tb_categories SET "
			+ "name = #{name},"
			+ "description = #{description}"
			+ " WHERE category_id = #{id}"
		)
	void updateCategory( Category category );
	

	@Insert("INSERT INTO tb_categories(name, description) values( #{name}, #{description})")
	void addCategory( Category category);
}
