package com.choeunchantra.home.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.choeunchantra.home.model.Article;

@Repository
public interface IArticleRepository {

	
	@Select("SELECT a.id, a.content, a.description, a.author, a.createdate, a.image, a.category_id , c.name, c.description AS category_des"
			+ " from tb_article a INNER JOIN tb_categories c ON a.category_id = c.category_id WHERE a.id = #{id}")
	
	@Results({
			@Result(property="id",column="id"),
			@Result(property="content",column="content"),
			@Result(property="description",column="description"),
			@Result(property="author",column="author"),
			@Result(property="createDate",column="createdate"),
			@Result(property="image",column="image"),
			@Result(property="category.id",column="category_id"),
			@Result(property="category.name",column="name"),
			@Result(property="category.description",column="category_des")
	})
	Article getArticle( int id );
	
	
	
	@Select("SELECT a.id, a.content, a.description, a.author, a.createdate, a.image, a.category_id , c.name, c.description AS c_des"
			+ " from tb_article AS a INNER JOIN tb_categories AS c ON a.category_id = c.category_id ORDER BY a.id")
	@Results({
			@Result(property="id",column="id"),
			@Result(property="content",column="content"),
			@Result(property="description",column="description"),
			@Result(property="author",column="author"),
			@Result(property="createDate",column="createdate"),
			@Result(property="image",column="image"),
			@Result(property="category.id",column="category_id"),
			@Result(property="category.name",column="name"),
			@Result(property="category.description",column="c_des")
	})
	List<Article> getAllArticles();
	
	@Delete("DELETE from tb_article where id = #{id}")
	void deleteArticle( int id );
	
	
	@Update("UPDATE tb_article SET "
			+ "content = #{content},"
			+ "description = #{description},"
			+ "author = #{author},"
			+ "createdate = #{createDate},"
			+ "image = #{image},"
			+ "category_id = #{category.id}"
			+ "WHERE id = #{id}"
		)
	void update( Article article );

	@Insert("INSERT INTO tb_article(content, description, author, createdate,image,category_id) values( #{content}, #{description},#{author},#{createDate},#{image},#{category.id})")
	void addArticle( Article article);
}
