package com.choeunchantra.home.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.choeunchantra.home.model.Category;


//@Repository
public class CategoryRrepository implements ICategoryRepository{

	public static int id=0;
	
	public static List<Category> categories = new ArrayList<>();

	static {
		initCategory();
	}
	public CategoryRrepository() {
		
	}
	
	@Override
	public Category getCategry(int id) {
		for( Category category : categories )
			if( category.getId() == id )
				return category;
		
		return null;
	}

	@Override
	public List<Category> getAllCategory() {
		// TODO Auto-generated method stub
		return categories;
	}

	@Override
	public void deleteCategory(int id) {
		// TODO Auto-generated method stub
		for( Category category : categories )
			if( category.getId() == id ) {
				categories.remove( category );
				return;
			}
	}

	@Override
	public void updateCategory(Category category) {
		// TODO Auto-generated method stub
		for( int i=0 ; i<categories.size() ; i++ ) {
			if( category.getId() == categories.get(i).getId() ) {
				categories.get(i).setName(category.getName());
				categories.get(i).setDescription(category.getDescription());
				return;
			}
		}
	}

	@Override
	public void addCategory(Category category) {
		// TODO Auto-generated method stub
		categories.add( category );
		id++;
	}
	
	
	private static void initCategory() {
		categories.add( new Category(id, "Java","Java Programming Language is one of the most powerfull language nowadays"));
		id++;
		categories.add( new Category(id, "Spring","Spring is J2EE Framework"));
		id++;
		categories.add( new Category(id, "Web","HTML CSS JAVASCRIPT JQUERY AJAX BOOTSTRAP"));
		id++;
	}
}
