package com.choeunchantra.home.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.choeunchantra.home.model.Article;


//@Repository
public class ArticleRepository implements IArticleRepository {

	public static int id=1;
	
	private List<Article> articles = new ArrayList<>();
	
	public ArticleRepository() {
		initArticle();
	}
	
	@Override
	public void update(Article article) {
		// TODO Auto-generated method stub
		for( int i=0 ; i<articles.size() ; i++ ) {
			if( article.getId() == articles.get(i).getId() ) {
				articles.get(i).setAuthor(article.getAuthor());
				articles.get(i).setContent(article.getContent());
				articles.get(i).setCreateDate(article.getCreateDate());
				articles.get(i).setDescription(article.getDescription());
				articles.get(i).setImage(article.getImage());
				articles.get(i).setCategory(article.getCategory() );
				return;
			}
		}
	}
	
	@Override
	public Article getArticle(int id) {
		for( Article article : articles) {
			if( article.getId() == id )
				return article;
		}
		return null;
	}

	@Override
	public List<Article> getAllArticles() {
		// TODO Auto-generated method stub
		return articles;
	}

	@Override
	public void deleteArticle(int id) {
//		// TODO Auto-generated method stub
		for( Article article : articles) {
			if( article.getId() == id ) {
				articles.remove(article);
				return;
			}
		}
				
	}

	public void addArticle( Article article ) {
		articles.add(article);
		id++;
	}
	
	private void initArticle() {
		for( int i=1; i<=10 ; i++ ){
			articles.add( new Article(i, "content" + i,"descrition"+i, "chantra", "12-jun-18" , CategoryRrepository.categories.get(2),"picture.jpg"));
			id++;
		}
	}
	
}
