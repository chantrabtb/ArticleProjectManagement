CREATE TABLE tb_article (
  id int auto_increment,
  content varchar,
  description varchar,
  author varchar,
  createdate varchar,
  image varchar,
  category_id int
);

CREATE TABLE tb_categories (
  category_id int auto_increment,
  name varchar,
  description varchar
);

INSERT INTO tb_categories VALUES (1, 'Java', 'Java a powerfull programming language');
INSERT INTO tb_categories VALUES (4, 'JPA', 'JPA is java framework');
INSERT INTO tb_categories VALUES (2, 'Web', 'Creating front end');

